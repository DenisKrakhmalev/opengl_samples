#version 330

uniform sampler2D depthTex; //Карта глубин
uniform sampler2DShadow depthTex_shadow; // Карта глубин как теневая текстура: для повышенной точности сэмплинга.
uniform sampler2D rotateTex; //Текстура со случайными значениям
uniform sampler2D normalMap; // Нормали в пространстве камеры (необязательно)

uniform mat4 projMatrix;
uniform mat4 projMatrixInverse;

uniform float attBias;
uniform float attScale;
uniform float radius;

uniform bool useNormalMap = false;

in vec2 texCoord; //текстурные координаты (интерполированы между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

//8 лучей из центра куба к его вершинам
vec4 rndTable[8] = vec4[8] 
(
	normalize(vec4 ( -0.5, -0.5, -0.5, 0.0 )),
	normalize(vec4 (  0.5, -0.5, -0.5, 0.0 )),
	normalize(vec4 ( -0.5,  0.5, -0.5, 0.0 )),
	normalize(vec4 (  0.5,  0.5, -0.5, 0.0 )),
	normalize(vec4 ( -0.5, -0.5,  0.5, 0.0 )),
	normalize(vec4 (  0.5, -0.5,  0.5, 0.0 )),
	normalize(vec4 ( -0.5,  0.5,  0.5, 0.0 )),
	normalize(vec4 (  0.5,  0.5,  0.5, 0.0 ))
);

void main()
{
	float mainFragDepth = texture(depthTex, texCoord).r;

	float att = 0.0;

    //спец. случай для точек на бесконечности (фон)
	if (mainFragDepth == 1.0f) {
		att = 1;
	}
	else {
        vec4 ndcCoords = vec4(vec3(texCoord, mainFragDepth) * 2.0 - 1.0, 1);
        // Получили положение центра фрагмента в СК камеры.
        // Внимание! Перспективное деление не выполнено.
        vec4 pos = projMatrixInverse * ndcCoords;

        //случайный вектор читаем из текстуры и интерпретируем, как нормаль для случайной плоскости
        vec3 rotateColor = texture(rotateTex, gl_FragCoord.xy * 0.25).rgb;
        vec3 plane = rotateColor * 2.0 - 1.0;

        // Когда будем добавлять сдвиг по лучу к точке, его нужно будет домножить на w-компоненту точки.
        float r = radius * pos.w;

        // Нормаль к точке поверхности в пространстве камеры.
        vec3 normal = vec3(0);
        if (useNormalMap) {
            // Полагаемся на то, что нормаль направлена на наблюдателя.
            normal = texture(normalMap, texCoord).rgb * 2 - 1;
        }

        for (int i = 0; i < 8; i++)
        {
            vec4 sampleRay = vec4(reflect(rndTable[i].xyz, plane), 0); //отражаем лучи от случайной плоскости. Получаем случайные лучи

            if (useNormalMap) {
                // Тривиальная обработка нормалей: перетягиваем лучи в положительную полусферу.
                sampleRay.xyz = normalize(mix(sampleRay.xyz, normal, 0.5f));
            }

            // То же самое, что projMatrix * (pos + sampleRay * r).
            vec4 sampleP = ndcCoords + projMatrix * (sampleRay * r);
            // После перспективного деления внутри функции запроса к теневой текстуре мы должны оказаться в диапазоне [0;1].
            sampleP.xyz = mix(sampleP.xyz, sampleP.www, 0.5f);

            //проверяем: точка находится внутри геометрии или снаружи
            att += textureProj(depthTex_shadow, sampleP);
        }

        att *= 0.125f;
        if (!useNormalMap) {
            // Этот трюк нужен, если мы сэмплируем также в гарантированно затенённой полусфере (отрицательной).
            // Но мы же перетянули лучи в положительную (они сонаправлены с нормалью).
            att = clamp((att + attBias) * attScale, 0.0, 1.0);
        }
	}

	fragColor = vec4(att, att, att, 1.0);
}
