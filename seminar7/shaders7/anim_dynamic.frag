#version 330

in vec2 texCoords;

out vec3 color;

uniform sampler2D samplerDiffuse;

void main() {
    vec3 diffuseColor = texture(samplerDiffuse, texCoords).rgb;
	color = diffuseColor;
}
